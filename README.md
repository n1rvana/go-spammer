# go-spammer

Several ways of reading data off of channels are shown in this code.  

Most of them block.  

So, if you have a go routine managing multiple channels, 
you want to use select.

If, however, you have a variable number of channels, say an array
of them, you want to use:
```
for _, channel := range channels {
    for 0 < len(channel) {
        data <- channel
    }
}
```
This code will not block on reading the channel, because it will
only attempt to read when there is info in the channel